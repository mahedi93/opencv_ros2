# Opencv_ros2

After cloning this repo, run the docker build by the following command

```
docker build -t opencv-webcam .
```

After a successfull build, run the images by the following code,

```
docker run -it -v $PWD:/app/ --device=/dev/video0:/dev/video0 -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY opencv-webcam bash

```


Directing to the python script file 
run the following script inside the container:

```
python3 script.py:
```

Thats, all 


#### Refernces 

1 .https://www.appsloveworld.com/docker/100/7/access-webcam-using-opencv-python-in-docker?utm_content=cmp-true

